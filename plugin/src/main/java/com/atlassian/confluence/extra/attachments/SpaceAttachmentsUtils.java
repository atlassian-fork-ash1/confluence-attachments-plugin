package com.atlassian.confluence.extra.attachments;

import com.atlassian.confluence.search.v2.InvalidSearchException;

import java.util.Set;

public interface SpaceAttachmentsUtils {

    int COUNT_ON_EACH_PAGE = 20;

    /**
     * Returns a {@link SpaceAttachments} object
     *
     * @param spaceKey         String of space key
     * @param pageNumber       indicate which batch of attachments to return in the list
     * @param totalAttachments number of attachments in the space
     * @param pageSize         number of attachments to be displayed in a page. if not specified, value will be 0
     * @param sortBy           String of sort by, possible value is name, createddate, date
     * @param fileExtension    String of file extension, allow user to filter extension type
     * @param labels           Set of label, allow user to filter by labels
     * @return SpaceAttachments
     * @throws com.atlassian.confluence.search.v2.InvalidSearchException if there is a problem searching
     */
    SpaceAttachments getAttachmentList(String spaceKey, int pageNumber, int totalAttachments, int pageSize,
                                       String sortBy, String fileExtension, Set<String> labels) throws InvalidSearchException;
}
