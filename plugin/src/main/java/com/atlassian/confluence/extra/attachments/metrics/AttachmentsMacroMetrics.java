package com.atlassian.confluence.extra.attachments.metrics;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.event.api.AsynchronousPreferred;
import com.atlassian.event.api.EventPublisher;
import com.google.common.base.Ticker;
import org.joda.time.Duration;

public class AttachmentsMacroMetrics {
    private final Timer searchAttachmentsTimer = new Timer(Ticker.systemTicker());
    private final Timer filterAndSortAttachmentsTimer = new Timer(Ticker.systemTicker());
    private final Timer templateModelBuildTimer = new Timer(Ticker.systemTicker());
    private final Timer templateRenderTimer = new Timer(Ticker.systemTicker());

    private int unfilteredAttachmentsCount = 0;
    private int filteredAttachmentsCount = 0;

    public void publishTo(EventPublisher eventPublisher) {
        eventPublisher.publish(buildEvent());
    }

    private Event buildEvent() {
        return new Event(
                searchAttachmentsTimer.duration(),
                unfilteredAttachmentsCount,
                filterAndSortAttachmentsTimer.duration(),
                filteredAttachmentsCount,
                templateModelBuildTimer.duration(),
                templateRenderTimer.duration()
        );
    }

    public AttachmentsMacroMetrics searchAttachmentsStart() {
        searchAttachmentsTimer.start();
        return this;
    }

    public AttachmentsMacroMetrics searchAttachmentsFinish(final int attachmentCount) {
        searchAttachmentsTimer.stop();
        unfilteredAttachmentsCount = attachmentCount;
        return this;
    }

    public AttachmentsMacroMetrics filterAndSortAttachmentsStart() {
        filterAndSortAttachmentsTimer.start();
        return this;
    }

    public AttachmentsMacroMetrics filterAndSortAttachmentsFinish(final int attachmentCount) {
        filterAndSortAttachmentsTimer.stop();
        filteredAttachmentsCount = attachmentCount;
        return this;
    }

    public AttachmentsMacroMetrics templateModelBuildStart() {
        templateModelBuildTimer.start();
        return this;
    }

    public AttachmentsMacroMetrics templateModelBuildFinish() {
        templateModelBuildTimer.stop();
        return this;
    }

    public AttachmentsMacroMetrics templateRenderStart() {
        templateRenderTimer.start();
        return this;
    }

    public AttachmentsMacroMetrics templateRenderFinish() {
        templateRenderTimer.stop();
        return this;
    }

    @AsynchronousPreferred
    @EventName("confluence.macro.metrics.attachments")
    public static class Event {
        private final Duration searchAttachmentsDuration;
        private final int unfilteredAttachmentsCount;
        private final Duration filterAndSortAttachmentsDuration;
        private final int filteredAttachmentsCount;
        private final Duration templateModelBuildDuration;
        private final Duration templateRenderDuration;

        public Event(final Duration searchAttachmentsDuration, final int unfilteredAttachmentsCount, final Duration filterAndSortAttachmentsDuration, final int filteredAttachmentsCount, final Duration templateModelBuildDuration, final Duration templateRenderDuration) {
            this.searchAttachmentsDuration = searchAttachmentsDuration;
            this.unfilteredAttachmentsCount = unfilteredAttachmentsCount;
            this.filterAndSortAttachmentsDuration = filterAndSortAttachmentsDuration;
            this.filteredAttachmentsCount = filteredAttachmentsCount;
            this.templateModelBuildDuration = templateModelBuildDuration;
            this.templateRenderDuration = templateRenderDuration;
        }

        public long getSearchAttachmentsMillis() {
            return searchAttachmentsDuration.getMillis();
        }

        public int getUnfilteredAttachmentsCount() {
            return unfilteredAttachmentsCount;
        }

        public long getFilterAndSortAttachmentsMillis() {
            return filterAndSortAttachmentsDuration.getMillis();
        }

        public int getFilteredAttachmentsCount() {
            return filteredAttachmentsCount;
        }

        public long getTemplateModelBuildMillis() {
            return templateModelBuildDuration.getMillis();
        }

        public long getTemplateRenderMillis() {
            return templateRenderDuration.getMillis();
        }
    }

}
