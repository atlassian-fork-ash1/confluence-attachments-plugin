package com.atlassian.confluence.extra.attachments.rest;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.extra.attachments.ImagePreviewRenderer;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("details")
public class DetailsResource {
    private final AttachmentManager attachmentManager;
    private final ImagePreviewRenderer imagePreviewRenderer;

    public DetailsResource(final AttachmentManager attachmentManager, final ImagePreviewRenderer imagePreviewRenderer) {
        this.attachmentManager = attachmentManager;
        this.imagePreviewRenderer = imagePreviewRenderer;
    }

    @Produces(MediaType.TEXT_HTML)
    @GET
    @Path("preview")
    public String getPreview(@QueryParam("attachmentId") long attachmentId) {
        final Attachment attachment = attachmentManager.getAttachment(attachmentId);
        if (attachment != null) {
            try {
                return getAttachmentPreviewHtml(attachment);
            } catch (XhtmlException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    private String getAttachmentPreviewHtml(Attachment attachment) throws XhtmlException {
        return imagePreviewRenderer.render(attachment, new DefaultConversionContext(attachment.getContainer().toPageContext()));
    }
}
