package com.atlassian.confluence.extra.attachments;

import com.atlassian.confluence.api.service.accessmode.AccessModeService;
import com.atlassian.confluence.content.render.image.ImageDimensions;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.datetime.FriendlyDateFormatter;
import com.atlassian.confluence.core.datetime.RequestTimeThreadLocal;
import com.atlassian.confluence.extra.attachments.metrics.AttachmentsMacroMetrics;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.descriptor.web.DefaultWebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.AttachmentComparator;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.user.User;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import static com.atlassian.confluence.renderer.ConfluenceRenderContextOutputType.PAGE_GADGET;
import static com.atlassian.confluence.util.GeneralUtil.getBuildNumber;
import static com.atlassian.confluence.util.HtmlUtil.htmlEncode;
import static com.atlassian.renderer.RenderContextOutputType.PDF;
import static com.atlassian.renderer.RenderContextOutputType.PREVIEW;
import static com.atlassian.renderer.v2.RenderUtils.blockError;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.StringEscapeUtils.escapeXml10;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Implements the {attachments} macro.
 */
public class AttachmentsMacro extends BaseMacro implements Macro, EditorImagePlaceholder {
    private final AttachmentManager attachmentManager;
    private final PermissionManager permissionManager;
    private final VelocityHelperService velocityHelperService;
    private final PageManager pageManager;
    private final I18NBeanFactory i18NBeanFactory;
    private final LocaleManager localeManager;
    private final FormatSettingsManager formatSettingsManager;
    private final UserAccessor userAccessor;
    private final PluginAccessor pluginAccessor;
    private final ImagePreviewRenderer imagePreviewRenderer;
    private final EventPublisher eventPublisher;
    private final AccessModeService accessModeService;

    private static final String SORTORDER_ASCENDING = "ascending";
    private static final String SORTORDER_DESCENDING = "descending";

    private ContentEntityObject contentObject;
    private PageContext pageContext;
    private static final String PLACEHOLDER_IMAGE_PATH = "/download/resources/confluence.extra.attachments/placeholder.png";

    public AttachmentsMacro(VelocityHelperService velocityHelperService, PermissionManager permissionManager,
                            AttachmentManager attachmentManager, PageManager pageManager, I18NBeanFactory i18NBeanFactory,
                            LocaleManager localeManager, FormatSettingsManager formatSettingsManager,
                            UserAccessor userAccessor, ImagePreviewRenderer imagePreviewRenderer,
                            PluginAccessor pluginAccessor, EventPublisher eventPublisher,
                            AccessModeService accessModeService) {
        this.velocityHelperService = velocityHelperService;
        this.permissionManager = permissionManager;
        this.attachmentManager = attachmentManager;
        this.pageManager = pageManager;
        this.i18NBeanFactory = i18NBeanFactory;
        this.localeManager = localeManager;
        this.formatSettingsManager = formatSettingsManager;
        this.userAccessor = userAccessor;
        this.imagePreviewRenderer = imagePreviewRenderer;
        this.pluginAccessor = pluginAccessor;
        this.eventPublisher = eventPublisher;
        this.accessModeService = accessModeService;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context) {
        return TokenType.BLOCK;
    }

    public boolean hasBody() {
        return false;
    }

    // irrelevant since we have no body
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    /**
     * Process a request to render an {attachments} macro
     */
    @Override
    public String execute(final Map<String, String> macroParameters, final String ignoredMacroBody, final ConversionContext conversionContext) {
        final AttachmentsMacroMetrics metrics = new AttachmentsMacroMetrics();

        final User user = AuthenticatedUserThreadLocal.get();

        final String pageTitle = macroParameters.get("page");
        if (isNotBlank(pageTitle)) {
            contentObject = getPage(conversionContext.getPageContext(), pageTitle);
            pageContext = new PageContext(contentObject);
            if (contentObject == null) {
                return blockError(getI18NBean().getText("confluence.extra.attachments.error.pagenotfound",
                        singletonList(htmlEncode(pageTitle))), "");
            }

            // check user permission
            if (!permissionManager.hasPermission(user, Permission.VIEW, contentObject)) {
                return blockError(getI18NBean().getText("confluence.extra.attachments.error.nopagepermission",
                        singletonList(htmlEncode(pageTitle))), "");
            }
        } else {
            // retrieve a reference to the body object this macro is in
            contentObject = conversionContext.getEntity();
            pageContext = conversionContext.getPageContext();
        }

        // if this macro is run from within a comment, use the underlying page to find the attachments
        if (contentObject instanceof Comment) {
            contentObject = ((Comment) contentObject).getContainer();
        }

        final HttpServletRequest servletRequest = ServletActionContext.getRequest();
        final Sorter sorter = Sorter.build(macroParameters, servletRequest);
        final List<Attachment> attachments = findAttachments(macroParameters, sorter, metrics);

        metrics.templateModelBuildStart();
        final Map<String, Object> contextMap = buildTemplateModel(macroParameters, conversionContext, user, attachments, sorter, servletRequest);
        metrics.templateModelBuildFinish();

        metrics.templateRenderStart();
        final String renderedOutput = velocityHelperService.getRenderedTemplate("templates/extra/attachments/attachmentsmacro.vm", contextMap);
        metrics.templateRenderFinish();

        metrics.publishTo(eventPublisher);

        return renderedOutput;
    }

    private Map<String, Object> buildTemplateModel(final Map<String, String> parameters, final ConversionContext conversionContext, final User user, final List<Attachment> attachments, final Sorter sorter, final HttpServletRequest servletRequest) {
        final Map<String, Object> contextMap = velocityHelperService.createDefaultVelocityContext();

        contextMap.put("latestVersionsOfAttachments", attachments);

        contextMap.put("hasAttachFilePermissions", hasAttachFilePermissions(user));
        contextMap.put("page", contentObject);

        // the reference to this macro allows the template to call getAttachmentDetails() (see below)
        contextMap.put("macro", this);

        contextMap.put("old", getBooleanParameter(parameters, "old", true));
        contextMap.put("preview", getBooleanParameter(parameters, "preview", true));
        contextMap.put("upload", getBooleanParameter(parameters, "upload", true));
        // CONFATT-65 : File upload controls do not work when macro is displayed in macro browser & preview area. We will hide the controls when it's displayed
        //              in macro browser. However due to https://jira.atlassian.com/browse/CONF-24923, we are unable to fix this when macro is displayed in preview
        contextMap.put("renderedInPreview", PREVIEW.equals(conversionContext.getOutputType()));
        contextMap.put("max", 5);
        contextMap.put("remoteUser", user);
        contextMap.put("attachmentsMacroHelper", this);
        contextMap.put("showActions", !PDF.equals(conversionContext.getOutputType()));
        contextMap.put("outputType", conversionContext.getOutputType());
        contextMap.put("macroParams", getMacroParametersWithSortByParamReadFromRequest(parameters, servletRequest));
        contextMap.put("uploadIFrameName", randomAlphanumeric(64));
        contextMap.put("sortBy", sorter.sortBy);
        contextMap.put("sortOrder", sorter.sortOrder);
        contextMap.put("newSortOrder", SORTORDER_DESCENDING.equals(sorter.sortOrder) ? SORTORDER_ASCENDING : SORTORDER_DESCENDING);
        ConfluenceUserPreferences pref = userAccessor.getConfluenceUserPreferences(user);
        contextMap.put("friendlyDateFormatter", new FriendlyDateFormatter(RequestTimeThreadLocal.getTimeOrNow(), pref.getDateFormatter(formatSettingsManager, localeManager)));

        contextMap.put("pdlEnabled", Long.parseLong(getBuildNumber()) >= 4000);
        contextMap.put("spaceKey", conversionContext.getSpaceKey());
        contextMap.put("previewsEnabled", pluginAccessor.isPluginEnabled("com.atlassian.confluence.plugins.confluence-previews"));
        contextMap.put("accessMode", accessModeService.getAccessMode().name());

        return contextMap;
    }

    private List<Attachment> findAttachments(final Map<String, String> macroParameters, final Sorter sorter, final AttachmentsMacroMetrics metrics) {
        // If the entities id == 0, the page object was newly created and is not persisted yet. Therefore does not have any attributes.
        if (contentObject != null && contentObject.getId() != 0) {
            // the old versions are retrieved by the template, so we only want the latest versions in this list
            metrics.searchAttachmentsStart();
            final List<Attachment> attachments = newArrayList(attachmentManager.getLatestVersionsOfAttachments(contentObject));
            metrics.searchAttachmentsFinish(attachments.size());

            metrics.filterAndSortAttachmentsStart();
            final List<Attachment> filteredAndSortedAttachments = attachments.stream()
                    .filter(attachmentFilenameFilter(macroParameters))
                    .filter(requiredLabelsFilter(macroParameters))
                    .sorted(sorter)
                    .collect(toList());
            metrics.filterAndSortAttachmentsFinish(filteredAndSortedAttachments.size());

            return filteredAndSortedAttachments;

        } else {
            return emptyList();
        }
    }

    private static Predicate<Attachment> attachmentFilenameFilter(final Map<String, String> macroParameters) {
        final String fileNamePatterns = macroParameters.get("patterns");
        if (fileNamePatterns != null) {
            final List<Pattern> patterns = compileFilenamePatterns(fileNamePatterns);
            return attachment -> patterns.stream()
                    .anyMatch(pattern -> pattern.matcher(attachment.getFileName()).matches());
        } else {
            return x -> true;
        }
    }

    private static Predicate<Attachment> requiredLabelsFilter(final Map<String, String> macroParameters) {
        final Collection<String> requiredLabels = getRequiredLabels(macroParameters);
        if (!requiredLabels.isEmpty()) {
            return attachment -> attachmentHasAllRequiredLabels(requiredLabels, attachment);
        } else {
            return x -> true;
        }
    }

    private static Collection<String> getRequiredLabels(final Map<String, String> macroParameters) {
        final String labelsString = macroParameters.get("labels");
        return !isBlank(labelsString) ? newHashSet(labelsString.split(",")) : emptySet();
    }

    private static boolean attachmentHasAllRequiredLabels(final Collection<String> requiredLabels, final Attachment attachment) {
        return !attachment.getLabels().isEmpty() &&
                requiredLabels.stream()
                        .map(String::trim)
                        .allMatch(requiredLabel -> hasLabel(attachment, requiredLabel)); // find any labels that the attachment does *not* have
    }

    private static boolean hasLabel(final Attachment attachment, final String requiredLabel) {
        return attachment.getLabels().stream()
                .anyMatch(label -> requiredLabel.equals(label.getName()));
    }

    private static List<Pattern> compileFilenamePatterns(final String fileNamePatterns) {
        return stream(fileNamePatterns.split(","))
                .map(String::trim)
                .map(pattern -> pattern.startsWith("*") ? "." + pattern : pattern)
                .map(Pattern::compile)
                .collect(toList());
    }

    private static class Sorter implements Comparator<Attachment> {
        @Nonnull
        final String sortBy;

        @Nullable
        final String sortOrder;

        private Sorter(final String sortBy, final String sortOrder) {
            this.sortBy = sortBy;
            this.sortOrder = sortOrder;
        }

        @Override
        public int compare(final Attachment o1, final Attachment o2) {
            return new AttachmentComparator(sortBy, SORTORDER_DESCENDING.equals(sortOrder)).compare(o1, o2);
        }

        static Sorter build(final Map<String, String> macroParameters, final HttpServletRequest servletRequest) {
            String sortBy = macroParameters.get("sortBy");
            String sortOrder = macroParameters.get("sortOrder");
            // check if any sortBy parameters were set by the request if available.
            if (servletRequest != null) {
                final String requestSortBy = servletRequest.getParameter("sortBy");
                if (requestSortBy != null) {
                    sortBy = requestSortBy;
                }
                final String requestSortOrder = servletRequest.getParameter("sortOrder");
                if (requestSortOrder != null) {
                    sortOrder = requestSortOrder;
                }
            }
            if (sortBy == null) {
                sortBy = "date";
            }
            return new Sorter(sortBy, sortOrder);
        }
    }

    private Boolean hasAttachFilePermissions(final User remoteUser) {
        try {
            return permissionManager.hasCreatePermission(remoteUser, contentObject, Attachment.class);
        } catch (IllegalStateException ex) {
            // the contentObject is probably a draft
            return false;
        }
    }

    public List<Attachment> getAllAttachmentVersions(Attachment attachment) {
        return attachmentManager.getAllVersions(attachment);
    }

    public boolean willBeRendered(final Attachment attachment) {
        return imagePreviewRenderer.willBeRendered(attachment);
    }

    public WebInterfaceContext getWebInterfaceContext(WebInterfaceContext context, Attachment attachment) {
        final DefaultWebInterfaceContext defaultContext = DefaultWebInterfaceContext.copyOf(context);
        defaultContext.setAttachment(attachment);

        // When in the page gadget there is no action to retrieve the page from.
        if (contentObject instanceof AbstractPage || PAGE_GADGET.toString().equals(pageContext.getOutputType())) {
            defaultContext.setPage((AbstractPage) contentObject);
        }
        return defaultContext;
    }

    private static Boolean getBooleanParameter(final Map<String, String> parameters, final String name, final boolean defaultValue) {
        return Optional.ofNullable(parameters.get(name))
                .map(Boolean::valueOf)
                .orElse(defaultValue);
    }

    private static Map<String, String> getMacroParametersWithSortByParamReadFromRequest(Map<String, String> macroParams, final HttpServletRequest servletRequest) {
        final Map<String, String> combinedParams = new HashMap<>(macroParams);

        Optional.ofNullable(servletRequest)
                .map(req -> req.getParameter("sortBy"))
                .filter(StringUtils::isNotBlank)
                .ifPresent(sortBy -> combinedParams.put("sortBy", sortBy));

        return combinedParams;
    }

    /**
     * Copied from ViewAttachmentsAction
     *
     * @param attachment The Attachment
     * @return String[]
     */
    public String[] getAttachmentDetails(Attachment attachment) {
        return new String[]{escapeXml10(attachment.getFileName()), String.valueOf(attachment.getVersion())};
    }

    public String execute(Map parameters, String body, RenderContext renderContext) {
        return execute(parameters, body, new DefaultConversionContext(renderContext));
    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    private ContentEntityObject getPage(PageContext context, String pageTitleToRetrieve) {
        if (isBlank(pageTitleToRetrieve)) {
            return context.getEntity();
        }

        String spaceKey = context.getSpaceKey();
        String pageTitle = pageTitleToRetrieve;

        int colonIndex = pageTitleToRetrieve.indexOf(":");
        if (colonIndex != -1 && colonIndex != pageTitleToRetrieve.length() - 1) {
            spaceKey = pageTitleToRetrieve.substring(0, colonIndex);
            pageTitle = pageTitleToRetrieve.substring(colonIndex + 1);
        }

        return pageManager.getPage(spaceKey, pageTitle);
    }

    private I18NBean getI18NBean() {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.get()));
    }

    @Override
    public ImagePlaceholder getImagePlaceholder(Map<String, String> stringStringMap, ConversionContext conversionContext) {
        return new DefaultImagePlaceholder(
                PLACEHOLDER_IMAGE_PATH, true, new ImageDimensions(310, 172)
        );
    }
}
