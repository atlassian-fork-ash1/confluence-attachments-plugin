package com.atlassian.confluence.extra.attachments;

import com.atlassian.confluence.api.model.accessmode.AccessMode;
import com.atlassian.confluence.api.service.accessmode.AccessModeService;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.Labelling;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.impl.DefaultUser;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.reverse;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestAttachmentsMacro {
    @Mock
    private VelocityHelperService velocityHelperService;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private PageManager pageManager;
    @Mock
    private I18NBeanFactory i18NBeanFactory;
    @Mock
    private LocaleManager localeManager;
    @Mock
    private UserAccessor userAccessor;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private FormatSettingsManager formatSettingsManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private ImagePreviewRenderer imagePreviewRenderer;
    @Mock
    private AccessModeService accessModeService;

    private Page pageToBeRendered;
    private ConfluenceUser remoteUser;
    private AttachmentsMacro attachmentsMacro;

    @Before
    public void setUp() {
        ServletActionContext.setRequest(httpServletRequest);

        remoteUser = new ConfluenceUserImpl(new DefaultUser("admin"));
        AuthenticatedUserThreadLocal.set(remoteUser);

        pageToBeRendered = new Page();
        pageToBeRendered.setId(1);

        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(new HashMap<>());
        when(userAccessor.getConfluenceUserPreferences(AuthenticatedUserThreadLocal.get())).thenReturn(new ConfluenceUserPreferences());
        when(formatSettingsManager.getTimeFormat()).thenReturn("h:mm a");
        when(formatSettingsManager.getDateFormat()).thenReturn("MMM dd, yyyy");
        when(formatSettingsManager.getDateTimeFormat()).thenReturn("MMM dd, yyyy HH:mm");
        when(accessModeService.getAccessMode()).thenReturn(AccessMode.READ_WRITE);
        attachmentsMacro = new AttachmentsMacro(velocityHelperService, permissionManager, attachmentManager, pageManager, i18NBeanFactory, localeManager, formatSettingsManager, userAccessor, imagePreviewRenderer, pluginAccessor, eventPublisher, accessModeService);
    }

    @After
    public void tearDown() {
        ContainerManager.getInstance().setContainerContext(null);
        AuthenticatedUserThreadLocal.reset();
        ServletActionContext.setRequest(null);
    }

    private static Attachment createAttachment(final String name, final long size, final Date lastModificationDate) {
        final Attachment attachment = new Attachment(name, "text/plain", size, EMPTY);

        attachment.setId(name.hashCode());
        attachment.setLastModificationDate(lastModificationDate);
        return attachment;
    }

    @Test
    public void testWithUnsortedAttachments() {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>(), "", pageToBeRendered.toPageContext());

        verifyTemplateRender(attachments);
    }

    @Test
    public void testWithUnsortedAttachmentsWhileShowingOldAttachmentsAndAllowingUpload() {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>() {
            {
                put("old", Boolean.TRUE.toString());
                put("upload", Boolean.TRUE.toString());
            }
        }, "", pageToBeRendered.toPageContext());

        verifyTemplateRender(attachments);
    }

    @Test
    public void testCommentOwnerContentProcessedInsteadOfTheCommentItselfWhenMacroIsUsedInComment() {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");
        final Comment comment = new Comment();

        comment.setContainer(pageToBeRendered);

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>(), "", comment.toPageContext());

        verifyTemplateRender(attachments);
    }

    @Test
    public void testAttachmentsSortedIfSortByParameterIsNotValid() {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(httpServletRequest.getParameter("sortBy")).thenReturn("invalidSortBy");
        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>(), "", pageToBeRendered.toPageContext());

        reverse(attachments);
        verifyTemplateRender(attachments);
    }

    @Test
    public void testWithAttachmentsSortedByName() {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>() {
            {
                put("sortBy", "name");
            }
        }, "", pageToBeRendered.toPageContext());

        reverse(attachments);
        verifyTemplateRender(attachments);
    }

    @Test
    public void testWithAttachmentsSortedByNameDescending() {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>() {
            {
                put("sortBy", "name");
                put("sortOrder", "descending");
            }
        }, "", pageToBeRendered.toPageContext());

        verifyTemplateRender(attachments);
    }

    @Test
    public void testWithAttachmentsSortedBySize() {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        attachments.get(0).setFileSize(30);
        attachments.get(1).setFileSize(50);

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>() {
            {
                put("sortBy", "size");
            }
        }, "", pageToBeRendered.toPageContext());

        verifyTemplateRender(attachments);
    }

    @Test
    public void testWithAttachmentsSortedBySizeDescending() {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt");

        attachments.get(0).setFileSize(30);
        attachments.get(1).setFileSize(50);

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>() {
            {
                put("sortBy", "size");
                put("sortOrder", "descending");
            }
        }, "", pageToBeRendered.toPageContext());

        reverse(attachments);
        verifyTemplateRender(attachments);
    }

    @Test
    public void testWithAttachmentsSortedByLastModifiedDate() {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>() {
            {
                put("sortBy", "date");
            }
        }, "", pageToBeRendered.toPageContext());

        verifyTemplateRender(attachments);
    }

    @Test
    public void testWithAttachmentsSortedByLastModifiedDateDescending() {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>() {
            {
                put("sortBy", "date");
                put("sortOrder", "descending");
            }
        }, "", pageToBeRendered.toPageContext());

        reverse(attachments);
        verifyTemplateRender(attachments);
    }

    @Test
    public void testSortParameterFromHttpRequestPreferredOverMacroParameter() {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt");

        when(httpServletRequest.getParameter("sortBy")).thenReturn("name");
        when(httpServletRequest.getParameter("sortOrder")).thenReturn("ascending");
        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>() {
            {
                put("sortBy", "date");
                put("sortOrder", "descending");
            }
        }, "", pageToBeRendered.toPageContext());

        verifyTemplateRender(attachments);
    }

    @Test
    public void testAttachmentsCanBeFilteredByCommaSeparatedRegexes() {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt", "file2.txt");

        when(httpServletRequest.getParameter("sortBy")).thenReturn("name");
        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);
        attachmentsMacro.execute(new HashMap<String, String>() {
            {
                put("patterns", "^file0\\.txt$,^file1\\.txt$");
            }
        }, "", pageToBeRendered.toPageContext());

        attachments.remove(2);
        verifyTemplateRender(attachments);
    }

    @Test
    public void testAttachmentsCanBeFilteredByLabel() {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt", "file2.txt");

        attachments.get(0).addLabelling(labelling("a"));
        attachments.get(1).addLabelling(labelling("a"));
        attachments.get(1).addLabelling(labelling("b"));
        attachments.get(2).addLabelling(labelling("b"));

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);
        attachmentsMacro.execute(singletonMap("labels", "a,b"), "", pageToBeRendered.toPageContext());

        verifyTemplateRender(singletonList(attachments.get(1)));
    }

    private Labelling labelling(final String labelText) {
        Label label = new Label(labelText);
        label.setId(labelText.hashCode());
        return new Labelling(label, null, (ConfluenceUser) null);
    }

    private void verifyTemplateRender(final List<Attachment> attachments) {
        final Map<String, Object> templateModel = verifyTemplateRenderAndGetModel();

        assertThat(templateModel, hasEntry("latestVersionsOfAttachments", attachments));
        assertThat(templateModel, hasEntry("hasAttachFilePermissions", true));
        assertThat(templateModel, hasEntry("page", pageToBeRendered));
        assertThat(templateModel, hasEntry("macro", attachmentsMacro));
        assertThat(templateModel, hasEntry("old", true));
        assertThat(templateModel, hasEntry("upload", true));
        assertThat(templateModel, hasEntry("max", 5));
        assertThat(templateModel, hasEntry("remoteUser", remoteUser));
        assertThat(templateModel, hasEntry("attachmentsMacroHelper", attachmentsMacro));
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> verifyTemplateRenderAndGetModel() {
        final ArgumentCaptor<Map> templateModelCaptor = ArgumentCaptor.forClass(Map.class);
        verify(velocityHelperService).getRenderedTemplate(ArgumentMatchers.anyString(), templateModelCaptor.capture());
        return templateModelCaptor.getValue();
    }

    private static List<Attachment> createAttachments(String... names) {
        List<Attachment> result = new ArrayList<>();

        final Calendar now = Calendar.getInstance();
        for (String name : names) {
            Attachment attachment = createAttachment(name, 0, now.getTime());
            result.add(attachment);
            now.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    @Test
    public void testAttachmentFileNameXmlEscapedByGetAttachmentDetails() {
        final Attachment attachment = createAttachment("'\"&<>", 0, new Date()); /* Attachment with XML unsafe characters */

        assertTrue(
                ArrayUtils.isEquals(
                        new String[]{StringEscapeUtils.escapeXml(attachment.getFileName()), String.valueOf(attachment.getVersion())},
                        attachmentsMacro.getAttachmentDetails(attachment)
                )
        );
    }

    @Test
    public void testMacroGeneratesBlockHtml() {
        assertEquals(Macro.OutputType.BLOCK, attachmentsMacro.getOutputType());
    }

    @Test
    public void testMacroHasNoBody() {
        assertEquals(Macro.BodyType.NONE, attachmentsMacro.getBodyType());
    }

    @Test
    public void testMacroDoesNotRenderBody() {
        assertEquals(RenderMode.NO_RENDER, attachmentsMacro.getBodyRenderMode());
    }

}
