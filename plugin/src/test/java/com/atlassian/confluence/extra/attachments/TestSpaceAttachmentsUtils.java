package com.atlassian.confluence.extra.attachments;

import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.search.v2.SearchManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;

public class TestSpaceAttachmentsUtils {
    @Mock
    private SearchManager searchManager;
    @Mock
    private AnyTypeDao anyTypeDao;

    private DefaultSpaceAttachmentsUtils defaultSpaceAttachmentsUtils;

    @Before
    public void setUp() {
        defaultSpaceAttachmentsUtils = new DefaultSpaceAttachmentsUtils(searchManager, anyTypeDao);
    }

    @Test
    public void testCalculateTotalPage() {
        assertEquals(3, defaultSpaceAttachmentsUtils.calculateTotalPage(26, 10));
        assertEquals(3, defaultSpaceAttachmentsUtils.calculateTotalPage(30, 10));
    }

    @Test
    public void testCalculateStartIndex() {
        assertEquals(10, defaultSpaceAttachmentsUtils.calculateStartIndex(2, 10));
        assertEquals(30, defaultSpaceAttachmentsUtils.calculateStartIndex(3, 15));
    }

}
