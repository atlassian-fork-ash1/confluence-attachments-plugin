package it.jwebunit;

import com.atlassian.confluence.it.AcceptanceTestHelper;
import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.SpacePermission;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.content.ViewAttachmentsBean;
import com.atlassian.confluence.it.content.ViewContentBean;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.it.user.LoginHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.atlassian.confluence.it.AcceptanceTestHelper.TEST_SPACE;
import static java.io.File.createTempFile;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertElementPresentByXPath;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertLinkPresentWithExactText;
import static net.sourceforge.jwebunit.junit.JWebUnit.getElementAttributeByXPath;
import static net.sourceforge.jwebunit.junit.JWebUnit.getElementTextByXPath;
import static net.sourceforge.jwebunit.junit.JWebUnit.setTextField;
import static net.sourceforge.jwebunit.junit.JWebUnit.setWorkingForm;
import static net.sourceforge.jwebunit.junit.JWebUnit.submit;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.lang.RandomStringUtils.random;
import static org.apache.log4j.lf5.util.StreamUtils.copy;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AttachmentsMacroTestCase {
    private static final String ATTACHMENTS_TABLE = "//div[contains(@class, 'plugin_attachments_table_container')]//table[contains(@class, 'attachments') and contains(@class, 'aui')]";
    private static final String ATTACHMENTS_TABLE_BODY = ATTACHMENTS_TABLE + "//tbody";
    private static final String ATTACHMENTS_TABLE_HEAD = ATTACHMENTS_TABLE + "//thead";

    private static final String FIRST_ATTACHMENT_LINK = ATTACHMENTS_TABLE_BODY + "//tr[contains(@class, 'attachment-row')]/td[2]//a";
    private static final String NAME_COLUMN_HEADER = ATTACHMENTS_TABLE_HEAD + "//th[2]//a";
    private static final String MODIFIED_COLUMN_HEADER = ATTACHMENTS_TABLE_HEAD + "//th[3]//a";

    private AcceptanceTestHelper helper = AcceptanceTestHelper.make();
    private TestName testName = new TestName();
    private ConfluenceRpc rpc;
    private ConfluenceRpc adminRpc;
    private LoginHelper loginHelper;

    @Before
    public void setUp() throws Exception {
        helper.setUp(AttachmentsMacroTestCase.class, testName);
        rpc = ConfluenceRpc.newInstance(helper.getBaseUrl(), ConfluenceRpc.Version.V2_WITH_WIKI_MARKUP); //old school wikimarkup!
        adminRpc = ConfluenceRpc.newInstance(helper.getBaseUrl());
        loginHelper = new LoginHelper(helper.getWebTester());

        rpc.logIn(User.TEST);
        adminRpc.logIn(User.ADMIN);
        adminRpc.grantPermission(SpacePermission.VIEW, TEST_SPACE, User.TEST);
        adminRpc.grantPermission(SpacePermission.PAGE_EDIT, TEST_SPACE, User.TEST);
        adminRpc.grantPermission(SpacePermission.ATTACHMENT_CREATE, TEST_SPACE, User.TEST);
    }

    @Test
    public void testAttachmentsFilteredByRegex() throws IOException {
        Page testPage = new Page(Space.TEST, "Page with regex filter", "{attachments:patterns=^shouldnotseeanyattachments.txt$}");
        Attachment attachment = new Attachment(testPage, "test.txt", "text/plain", "", "Test content".getBytes("UTF-8"));

        rpc.createPage(testPage);
        rpc.createAttachment(attachment);
        adminRpc.flushIndexQueue();

        loginHelper.logInFast(User.TEST);

        ViewContentBean viewPage = ViewContentBean.viewPage(testPage);
        assertTrue(viewPage.getContent().contains("No files shared here yet."));
    }

    @Test
    public void testFileUpload() throws IOException {
        File tempFile = createTempFile("testFileUpload", ".txt");
        writeByteArrayToFile(tempFile, random(32).getBytes("UTF-8"));

        Page testPage = new Page(Space.TEST, "Page with upload", "{attachments:upload=true}");
        rpc.createPage(testPage);

        loginHelper.logInFast(User.ADMIN);
        ViewContentBean viewPage = ViewContentBean.viewPage(testPage);

        setWorkingForm(1);
        setTextField("file_0", tempFile.getAbsolutePath());
        submit("confirm");

        assertEquals(
                tempFile.getName(),
                getElementAttributeByXPath(FIRST_ATTACHMENT_LINK, "data-filename")
        );

        assertTrue(tempFile.delete());
    }

    private File copyClassPathResourceToFile(String classpathResource, String fileNameSuffix) throws IOException {
        InputStream in = null;
        OutputStream out = null;

        try {
            File tempFile = createTempFile("it.com.atlassian.confluence.extra.attachments", fileNameSuffix);

            in = new BufferedInputStream(getClass().getClassLoader().getResourceAsStream(classpathResource));
            out = new BufferedOutputStream(new FileOutputStream(tempFile));

            copy(in, out);
            return tempFile;
        } finally {
            closeQuietly(out);
            closeQuietly(in);
        }
    }

//    /**
//     * <a href="http://developer.atlassian.com/jira/browse/CONFATT-4">CONFATT-4</a>
//     * @throws IOException
//     * Thrown if there is an error copying class path resource &quot;CONFATT-4-site-export.zip&quot; to the temporary
//     * directory for the data restore operation.
//     */
//    public void testOpenWebFolderLinkNotShownIfAttachmentsStoredInWebdavServer() throws IOException
//    {
//        File confatt4SiteExport = copyClassPathResourceToFile("CONFATT-4-site-export.zip", ".zip");
//
//        try
//        {
//            /* Restore a site backup that contains a global settings with the WebDAV server URL set */
//            getConfluenceWebTester().restoreData(confatt4SiteExport);
//
//            long testPageId = createTestPage("ds", getName(), "{attachments}");
//
//            viewPage(testPageId);
//            assertLinkNotPresentWithText("Open Web Folder");
//        }
//        finally
//        {
//            assertTrue(confatt4SiteExport.delete());
//        }
//    }
//

    @Test // This is important for CONFATT-6
    public void testErrorDivPresentOnConfluenceBuiltInAttachmentsUploadPageIfThereWereErrors() {
        Page testPage = new Page(Space.TEST, "Some Page", "");
        rpc.createPage(testPage);

        loginHelper.logInFast(User.TEST);
        ViewAttachmentsBean.viewAttachments(testPage);

        setWorkingForm("upload-attachments");
        submit();

        assertElementPresentByXPath("//div[contains(@class, 'aui-message') and contains(@class, 'error')]");
    }

    @Test
    public void testViewAttachmentsFromOtherPage() throws IOException {
        final String testContentString = "Test content";

        Page pageWithMacro = new Page(Space.TEST, "Page with attachments macro", "{attachments:page=testViewAttachmentsFromOtherPage}");
        Page pageWithAttachment = new Page(Space.TEST, "testViewAttachmentsFromOtherPage", "");
        Attachment attachment = new Attachment(pageWithAttachment, "test.txt", "text/plain", "", testContentString.getBytes("UTF-8"));

        rpc.createPage(pageWithAttachment);
        rpc.createAttachment(attachment);
        rpc.createPage(pageWithMacro);

        loginHelper.logInFast(User.TEST);
        ViewContentBean.viewPage(pageWithMacro);


        assertAttachmentsTableColumns();

        assertEquals("test.txt", getElementAttributeByXPath(FIRST_ATTACHMENT_LINK, "data-filename"));
    }

    @Test
    public void testViewAttachmentsFromOtherSpace() throws IOException {
        final String testContentString = "Test content";

        Space anotherSpace = new Space("AA", "Another");

        Page pageWithMacro = new Page(Space.TEST, "Page with attachments macro", "{attachments:page=AA:testViewAttachmentsFromOtherSpace}");
        Page pageWithAttachment = new Page(anotherSpace, "testViewAttachmentsFromOtherSpace", "");
        Attachment attachment = new Attachment(pageWithAttachment, "test.txt", "text/plain", "", testContentString.getBytes("UTF-8"));

        rpc.createSpace(anotherSpace);
        rpc.createPage(pageWithAttachment);
        rpc.createAttachment(attachment);
        rpc.createPage(pageWithMacro);

        loginHelper.logInFast(User.ADMIN);
        ViewContentBean.viewPage(pageWithMacro);

        assertAttachmentsTableColumns();

        assertEquals("test.txt", getElementAttributeByXPath(FIRST_ATTACHMENT_LINK, "data-filename"));
    }

    @Test
    public void testLongFilenamesNotTruncated() throws IOException {
        final String longAttachmentName = "The quick brown fox jumps over the lazy dog and she sells sea shells on the sea shore.txt";
        Page pageWithMacro = new Page(Space.TEST, "Page with attachments macro", "{attachments}");
        Attachment attachment = new Attachment(pageWithMacro, longAttachmentName, "text/plain", "", "test content".getBytes("UTF-8"));

        rpc.createPage(pageWithMacro);
        rpc.createAttachment(attachment);

        loginHelper.logInFast(User.TEST);
        ViewContentBean.viewPage(pageWithMacro);

        assertLinkPresentWithExactText(longAttachmentName);
    }

    private void assertAttachmentsTableColumns() {
        assertEquals("File", getElementTextByXPath(NAME_COLUMN_HEADER));
        assertEquals("Modified", getElementTextByXPath(MODIFIED_COLUMN_HEADER));
    }
}
