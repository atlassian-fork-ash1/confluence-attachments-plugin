package it.webdriver.pageobjects;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.webdriver.pageobjects.page.content.EditAttachmentPage;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * A custom subclass of the standard {@link EditAttachmentPage} which adds assertions we need for this test.
 * Should ideally be part of the original class.
 */
public class EditAttachmentPageWithTitle extends EditAttachmentPage {
    private final Content attachment;

    public EditAttachmentPageWithTitle(Content attachment) {
        super(attachment);
        this.attachment = attachment;
    }

    public void assertTitleMatchesAttachment() {
        assertThat(driver.getTitle(), allOf(
                containsString("Properties"),
                containsString(attachment.getTitle())
        ));
    }
}
