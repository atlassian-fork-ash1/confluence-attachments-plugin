package it.webdriver;

import com.atlassian.confluence.api.model.content.AttachmentUpload;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.test.api.model.person.UserWithDetails;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rpc.api.permissions.SpacePermission;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import it.webdriver.pageobjects.AttachmentSummary;
import it.webdriver.pageobjects.AttachmentsMacroComponent;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;

import static com.atlassian.confluence.api.model.content.ContentRepresentation.WIKI;
import static com.atlassian.confluence.api.model.content.ContentType.PAGE;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.PAGE_EDIT;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.google.common.io.Files.write;

@RunWith(ConfluenceStatelessTestRunner.class)
public class AttachmentsWithHistoryWebDriverTest {
    @Rule
    public TestName testName = new TestName();
    @Rule
    public MacroParametersRule macroParametersRule = new MacroParametersRule();

    @Fixture
    private static UserFixture user1 = userFixture().build();
    @Fixture
    private static UserFixture user2 = userFixture().build();

    @Fixture
    private static SpaceFixture spaceFixture = spaceFixture()
            .permission(user1, SpacePermission.ATTACHMENT_CREATE, SpacePermission.ATTACHMENT_REMOVE, SpacePermission.VIEW, PAGE_EDIT)
            .permission(user2, SpacePermission.VIEW, SpacePermission.ATTACHMENT_CREATE)
            .build();

    @Inject
    private static ConfluenceRestClient restClient;
    @Inject
    private static ConfluenceTestedProduct product;

    private static AttachmentDownloadTestUtils testUtils;

    @BeforeClass
    public static void initTestUtils() {
        testUtils = new AttachmentDownloadTestUtils(restClient);
    }

    private Content page;
    private Content version1;
    private static final String TEST_FILE_CONTENTS = "testing";

    @Before
    public void setUp() throws Exception {
        page = restClient.createSession(user1.get()).contentService().create(Content.builder(PAGE)
                .title(testName.getMethodName())
                .space(spaceFixture.get())
                .body(buildAttachmentMacroDescriptor(macroParametersRule.getMacroParameter()), WIKI)
                .build()
        );

        version1 = restClient.createSession(user1.get()).attachmentService().addAttachments(
                page.getId(), Collections.singleton(
                        new AttachmentUpload(createTempFile(TEST_FILE_CONTENTS), "test.txt", "text/plain", "Test file", false)
                )
        ).getResults().iterator().next();
    }

    private File createTempFile(String content) throws IOException {
        final File tempFile = Files.createTempFile(testName.getMethodName(), null).toFile();
        write(content.getBytes(), tempFile);
        return tempFile;
    }

    @Test
    public void multipleAttachmentVersions() throws Exception {
        final Content version2 = restClient.createSession(user2.get()).attachmentService().updateData(version1.getId(), new AttachmentUpload(
                createTempFile("For the better"), version1.getTitle(), version1.getType().getType(), "I've changed", false)
        );
        final Content version3 = restClient.createSession(user1.get()).attachmentService().updateData(version2.getId(), new AttachmentUpload(
                createTempFile("Not so good now"), version1.getTitle(), version1.getType().getType(), "I've changed once more", false)
        );

        loginAndViewAttachmentsMacro(user1.get())
                .assertAttachmentRowIsDisplayed(version3, user1.get())
                .openAttachmentSummary(version1)
                .assertAttachmentVersionIsDisplayed(1, false, user1.get(), "Test file")
                .assertAttachmentVersionIsDisplayed(2, false, user2.get(), "I've changed")
                .assertAttachmentVersionIsDisplayed(3, true, user1.get(), "I've changed once more");
    }

    @Test
    @MacroParameter(name = "old", value = "false")
    public void oldVersionsNotDisplayed() throws Exception {
        final Content version2 = restClient.createSession(user1.get()).attachmentService().updateData(version1.getId(), new AttachmentUpload(
                createTempFile("For the better"), version1.getTitle(), version1.getType().getType(), "I've changed", false)
        );

        loginAndViewAttachmentsMacro(user1.get())
                .assertAttachmentRowIsDisplayed(version2, user1.get())
                .openAttachmentSummary(version1)
                .assertVersionHistoryNotPresent();
    }

    // CONFDEV-15288
    @Test
    public void oldVersionsAreDisplayedAfterUpload() throws Exception {
        // First assert that we see the history on page load
        final ViewPage viewPage = product.loginAndView(user1.get(), page);
        AttachmentsMacroComponent macroComponent = getAttachmentsMacroComponent(viewPage);
        macroComponent.assertAttachmentRowIsDisplayed(version1, user1.get())
                .openAttachmentSummary(version1)
                .assertVersionHistoryPresent();

        // 'Fake' upload a new attachment version and trigger the refresh
        final Content version2 = restClient.createSession(user1.get()).attachmentService().updateData(version1.getId(), new AttachmentUpload(
                createTempFile("For the better"), version1.getTitle(), version1.getType().getType(), "I've changed", false)
        );

        macroComponent.refreshAttachments(page.getId(), version2);

        // Version history should be present on the newly-refreshed Attachment line
        macroComponent = getAttachmentsMacroComponent(viewPage);
        macroComponent.openAttachmentSummary(version1)
                .assertVersionHistoryPresent();
    }

    @Test
    public void downloadOldAttachmentVersion() throws Exception {
        restClient.createSession(user1.get()).attachmentService().updateData(version1.getId(), new AttachmentUpload(
                createTempFile("For the better"), version1.getTitle(), version1.getType().getType(), "I've changed", false)
        );

        final AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro(user1.get()).openAttachmentSummary(
                version1);

        testUtils.fetchUrlAndCheckContent(TEST_FILE_CONTENTS, attachmentSummary.getVersion(1).getAttachmentVersionDownloadUrl(), user1.get());
        testUtils.fetchUrlAndCheckContent("For the better", attachmentSummary.getVersion(2).getAttachmentVersionDownloadUrl(), user1.get());
    }

    private AttachmentsMacroComponent loginAndViewAttachmentsMacro(UserWithDetails user) {
        final ViewPage viewPage = product.loginAndView(user, page);
        return getAttachmentsMacroComponent(viewPage);
    }

    private AttachmentsMacroComponent getAttachmentsMacroComponent(ViewPage viewPage) {
        return viewPage.getComponent(AttachmentsMacroComponent.class, page);
    }

    private static String buildAttachmentMacroDescriptor(MacroParameter macroParameter) {
        String paramString = "";
        if (macroParameter != null) {
            paramString = String.format(":%s=%s", macroParameter.name(), macroParameter.value());
        }
        return String.format("{attachments%s}", paramString);
    }
}