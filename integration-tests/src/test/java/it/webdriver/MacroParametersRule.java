package it.webdriver;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class MacroParametersRule extends TestWatcher {
    private MacroParameter macroParameter;

    @Override
    protected void starting(Description description) {
        macroParameter = description.getAnnotation(MacroParameter.class);
    }

    public MacroParameter getMacroParameter() {
        return macroParameter;
    }
}
